 // Initial Setup
 // -------------

 // Setting the namespace in case it wasn't set up before
var smart;
if (smart === undefined) {
	smart = {};
}

smart.ExternalServices = function(){
	this.initialize();
};

smart.ExternalServices.prototype = {

	initialize: function()
	{
		_.bindAll(this, "initialize", "registerService", "runService", "fetchFoursquareVenuesNearby");
		this._services = {};

		this.registerService("foursquare_venues", 2, this.fetchFoursquareVenuesNearby);
	},

	registerService: function(service_name, no_params, method){
		this._services[service_name] = {"total_params":no_params, "execute": method};
	},

	runService: function(service_name, params, callback){
		if (service_name === undefined){
			throw new Error("First arg must be a Non Empty String");
		}
		var service = this._services[service_name];
		if (service === undefined){
			throw new Error("There is no such service registered");
		}
		if ( params===undefined && service.total_params !== 0 ){
			throw new Error("Sent parameters don't match with needed parameters. Sent: NULL Need: " + service.total_params);
		}
		if (params.length !== service.total_params) {
			throw new Error("Sent parameters don't match with needed parameters. Sent: " + params.length + " Need: "+ service.total_params);
		}
		service.execute(params, callback);
	},

	fetchFoursquareVenuesNearby: function(params, callback){
		var date = new Date();
		var date_string = moment(date).format('YYYYMMDD');
		var tmp_url = "https://api.foursquare.com/v2/venues/search?ll="+params[0]+","+params[1]+"&client_id=QFZZGPJPRCZZNHX1PXOBBH5K550B4UOZ0JA1ZCDWA3SUQ1JM&client_secret=ZVD0AOEAJZOW4QP2UX3MAR0LN0IR5GGBJJFPPD3ZMHBJMMZH&v="+date_string;
		jQuery.ajax({
			type: "GET",
			dataType: "jsonp",
			url: tmp_url,
			success: function(data){
				if (typeof(callback) == "function"){
					callback(data.response)
				}
			},
		});
	},
}

smart.externalServices = new smart.ExternalServices();
