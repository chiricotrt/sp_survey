 // Initial Setup
 // -------------

 // Setting the namespace in case it wasn't set up before
var smart;
if (smart === undefined) {
	smart = {};
}

smart.Validator = function(){
	this.initialize();
};

smart.Validator.prototype = {

	initialize: function()
	{
		_.bindAll(this, "initialize", "bindFields", "runCallbacks", "run",
						"validateSpeeding", "validateSpeedingCurrentNext", "validateSpeedingCurrentPrevious",
						"validateActivityDuration", "frequentHomeLocation");

		this._validatorList = {};
		this._methodList = {};

		this.bindFields(['start_time', 'end_time'], "validateCurrentStopTimeConsistency", 1000, "Activity start time needs to be before activity end time");
		this.bindFields(['start_time', 'end_time'], "validateCurrentAndPreviousStopTimeConsistency", 1001, "Activities times are overlapping");
		this.bindFields(['start_time', 'end_time'], "validateCurrentAndNextStopTimeConsistency", 1002, "Activities times are overlapping");
		this.bindFields(['start_time', 'end_time'], "validateZeroTimeActivities", 1003, "An activity can't have the same start and end time");

		this.bindFields(['start_time', 'end_time', "activity_location", "mode"], "validateSpeedingCurrentPrevious", 1010, "Improbable speed reached with previous stop");
		this.bindFields(['start_time', 'end_time', "activity_location"], "validateSpeedingCurrentNext", 1011, "Improbable speed reached with next stop");

		this.bindFields(['start_time', 'end_time', 'activity'], "validateActivityDuration", 1020, "Start/end times may be incorrect for the chosen activity");
		this.bindFields(['activity'], "frequentHomeLocation", 1021, "Activity marked as home out of usual home. Maybe Other's home?");

		this.bindFields(['start_time', 'end_time'], "validateZeroTimeTravels", 1030, "A travel activity can't have zero time duration");

		this.bindFields(['activity_location'], "validateMarkerPosition", 1040, "A marker can't be moved more than 500m");
	},


	// deactivateAll
	// Deactivates all registered validators
	deactivateAll: function(){
		for (var key in this._methodList){
			this._methodList[key].active = false;
		}
	},

	// activateAll
	// Activates all registered validators
	activateAll: function(){
		for (var key in this._methodList){
			this._methodList[key].active = true;
		}
	},

	setStatus: function(method_name, status){
		this._methodList[method_name].active = status;
	},

	// bindFields
	// ----------
	// Bind an array of fields to callback method,
	// and set the error code and message it should return
	// if the validation callback returns false.
	bindFields: function(fields, method, errorCode, message)
	{
		for(var i=0;i<fields.length;i++){
			var idx = fields[i];
			if (this._methodList[method] === undefined){
				this._methodList[method] = {active: true};
			}
			if (this._validatorList[idx] === undefined )
				this._validatorList[idx] = [];
			this._validatorList[idx].push({ 'callbackName': method, 'errorCode': errorCode, 'message': message });
		}

	},

	// run
	// ----------
	// Entry point to execute all the validations

	// Before executing the validations, we make sure some parameters are defined
	run: function(fields, current_stop, last_stop, next_stop)
	{
		if (fields == null || (Object.prototype.toString.call( fields ) === '[object Array]'  && fields.length == 0)) {
			throw new Error("First arg must be a Non Empty Array");
		} else if (current_stop === undefined) {
			throw new Error("Second arg (current_stop) must not be null");
		} else {
			return this.runCallbacks(fields, current_stop, last_stop, next_stop);
		}
	},

	// runCallbacks
	// ----------
	// Go through all fields and call the binded callbacks
	runCallbacks: function(fields, current_stop, last_stop, next_stop)
	{
		var returnErrors = [];
		for(var i=0;i<fields.length;i++){
			var idx = fields[i];
			var fieldValidators = this._validatorList[idx];

			if (fieldValidators===undefined){
				continue;
			}

			var validator_result;
			for(var j=0;j<fieldValidators.length; j++){
				if (this._methodList[fieldValidators[j].callbackName].active && !this[fieldValidators[j].callbackName].call(this,	 current_stop, last_stop, next_stop)){
					validator_result = {'errorCode': fieldValidators[j].errorCode, 'message': fieldValidators[j].message};

					if(_.find(returnErrors, function(elm){ return elm.errorCode === validator_result.errorCode }) === undefined){
						returnErrors.push(validator_result);
					}
				}
			}
		}
		return returnErrors;
	},

	// Validators
	// ---------

	// ### CurrentStopTimeConsistency
	validateCurrentStopTimeConsistency: function(current_stop)
	{
		if (current_stop.start_time.indexOf('-1') === 0) return true;
		if (current_stop.end_time.indexOf('+1') === 0) return true;

		if (current_stop.start_time > current_stop.end_time){
			return false;
		} else {
			return true;
		}
	},

	// ### CurrentAndPreviousStopTimeConsistency
	// Check if current and previous stop don't overlap
	validateCurrentAndPreviousStopTimeConsistency: function(current_stop, previous_stop)
	{
		//console.log("--", current_stop, previous_stop);
		if (!previous_stop) {
			//console.log("Checked PreviousStopTimeConsistency with previous_stop as undefined");
			return true;
		} else if (!smart.utils.doesObjContains(['start_time', 'end_time'], [current_stop])) {
			console.log("missing start or end time in current stop");
			return true;
		} else if (!smart.utils.doesObjContains(['start_time', 'end_time'], [previous_stop])) {
			console.log("missing start or end time in previous stop");
			return true;
		}

		if (current_stop.start_time.indexOf('-1') === 0) return true;
		if (current_stop.start_time > previous_stop.end_time){
			return true;
		} else {
			return false;
		}
	},

	// ### CurrentAndNextStopTimeConsistency
	// Check if current and next stop don't overlap
	validateCurrentAndNextStopTimeConsistency: function(current_stop, previous_stop, next_stop)
	{
		if (!next_stop) {
			//console.log("Checked CurrentAndNextStopTimeConsistency with next_stop as undefined");
			return true;
		} else if (!smart.utils.doesObjContains(['start_time', 'end_time'], [current_stop])) {
			console.log("missing start or end time in current stop");
			return true;
		} else if (!smart.utils.doesObjContains(['start_time', 'end_time'], [next_stop])) {
			console.log("missing start or end time in next stop");
			return true;
		}

		if (current_stop.end_time.indexOf('+1') === 0) return true;
		if (next_stop.start_time > current_stop.end_time){
			return true;
		} else {
			return false;
		}
	},

	// ### ZeroTimeActivities
	// We don't allow activities of zero minutes
	validateZeroTimeActivities: function(current_stop)
	{
		if (current_stop.start_time === current_stop.end_time){
			return false;
		} else {
			return true;
		}
	},

	// ### SpeedingPreviousCurrent
	validateSpeeding: function(stop_with_mode, stop_to_compare){
		var previous_travel, next_travel;
		var previous_mode, previous_mode_parts, next_mode, next_mode_parts;

		// console.log("stop_with_mode", stop_with_mode);
		if(stop_with_mode.mode){
			previous_mode_parts = stop_with_mode.mode.split("-");
			previous_mode = previous_mode_parts[0]+"-"+previous_mode_parts[1];
		} else {
			previous_mode="";
			// console.log("No mode declared")
		}
		return  !this.checkSpeeding(previous_mode, stop_to_compare, stop_with_mode);
	},

	validateSpeedingCurrentNext: function(current_stop, previous_stop, next_stop){
		console.log("validate speeding current next");
		if (!next_stop){
			console.log("NO NEXT STOP");
			return true;
		} else if (!smart.utils.doesObjContains(['stop_lat', 'stop_lng'], [current_stop, next_stop]) ) {
			console.log("MISSING parameters");
			return true;
			//throw new Error("Current Activity and Next Activity need to have latitude and longitude properties");
		} else if (!next_stop.hasOwnProperty("mode") ){
			console.log("TRAVEL DOESNOT HAVE A MODE");
			return true;
			//throw new Error("Next Activity needs to have mode property declared.");
		} else {
			//console.log("SPEEDING CHECK");
			return this.validateSpeeding(next_stop, current_stop);
		}

		//console.log("end validate speeding current next");
	},

	validateSpeedingCurrentPrevious: function(current_stop, previous_stop, next_stop){
		if (!previous_stop){
			return true;
		} else if ( !smart.utils.doesObjContains(['stop_lat', 'stop_lng'], [current_stop, previous_stop]) ){
			return true;
			//throw new Error("Current Activity and Next Activity need to have latitude and longitude properties");
		} else if (!current_stop.hasOwnProperty("mode") ){
			return true;
			//throw new Error("Next Activity needs to have mode property declared.");
		} else {
			return this.validateSpeeding(current_stop, previous_stop);
		}
		//console.log("end validate speeding current previous");
	},


	checkSpeeding: function (current_mode, prev_stop, next_stop){
		//console.log("SPEEDING CHECK", arguments);

		/*if(prev_stop.index===undefined || next_stop.index ===undefined){
			console.log("There is no travel segment here");
			return false;
		}*/
		var max_avg_speed;
		if (!current_mode || smart.Diary._modesByImage[current_mode]["max_avg_speed"] == null || smart.Diary._modesByImage[current_mode]["max_avg_speed"] == ""){
			max_avg_speed = 10000;
		}
		else{
			max_avg_speed = smart.Diary._modesByImage[current_mode]["max_avg_speed"];
		}

		var travelDuration = smart.utils.getMomentDuration(prev_stop['end_time'], next_stop['start_time']);
		var travelTime = travelDuration.asSeconds();

		var travelStartG = new google.maps.LatLng(prev_stop['stop_lat'], prev_stop['stop_lng']);
		var travelEndG = new google.maps.LatLng(next_stop['stop_lat'], next_stop['stop_lng']);

		//calc distance returns distance in KMs. Converting to M
		var traveledDistance = calcDistance(travelStartG, travelEndG) * 1000;
		//console.log('SPEEDING CHECK', traveledDistance, travelTime);
		var temp = (traveledDistance/travelTime > max_avg_speed);
		//console.log(temp);
		return temp;
	},

	validateActivityDuration: function (current_stop){
		//check for main activity
		var mainactivity = current_stop["mainactivity"];

		if (mainactivity === undefined){
			return true
		}
		else{
			return this.durationWithinBoundaries(mainactivity, current_stop);
		}

	},

	frequentHomeLocation: function (current_stop) {
		var mainactivity = current_stop["mainactivity"];
		//console.log("FREQUENT HOME LOCATION: ", current_stop);
		if (smart.Diary._frequent_home !== undefined){
			var p1 = new google.maps.LatLng(parseFloat(current_stop.stop_lat,10), parseFloat(current_stop.stop_lng,10));
			var p2 = new google.maps.LatLng(parseFloat(smart.Diary._frequent_home.lat,10) , parseFloat(smart.Diary._frequent_home.lon, 10));
			//console.log("dist",calcDistance(p1, p2));
			if (mainactivity === "activity-home" && calcDistance(p1, p2) > 1){
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	},

	durationWithinBoundaries: function(mainactivity, current_stop){
		var min_time,max_time;
		//console.log("DURATION WITHIN BOUNDARIES? ", mainactivity);
		//console.log("DURATION WITHIN BOUNDARIES? ", current_stop);
		if (mainactivity === undefined || mainactivity === "" || mainactivity === "activity-default"){
			min_time = 0;
			max_time = 10000;
		}
		else{
			var min_time = smart.Diary._activitiesByImage[mainactivity].min_time;
			var max_time = smart.Diary._activitiesByImage[mainactivity].max_time;
		}

		//if boundaries not defined, define with extreme limits
		if (min_time === undefined || min_time === '')
			min_time = 0;

		if (max_time === undefined || max_time === '')
			max_time = 10000;

		var activityMomentDuration = smart.utils.getMomentDuration(current_stop['start_time'], current_stop['end_time']);
		var activityDuration = activityMomentDuration.asMinutes();
		//console.log("ACTIVITY DURATION: ", activityDuration);
		//console.log("BOUNDARIES min: ", min_time, " MAX: ", max_time);
		return (activityDuration < max_time && activityDuration > min_time)
	},

	validateZeroTimeTravels: function(current_stop, previous_stop){
		if (!current_stop || !previous_stop)
			return true;

		if (previous_stop.end_time === current_stop.start_time){
			return false;
		} else {
			return true;
		}
	},

	validateMarkerPosition: function(current_stop){
		if (current_stop.previous_stop_lat === undefined || current_stop.previous_stop_lng === undefined)
			return true;

		var p1 = new google.maps.LatLng(current_stop.stop_lat, current_stop.stop_lng);
		var p2 = new google.maps.LatLng(current_stop.previous_stop_lat, current_stop.previous_stop_lng);
		var dist = calcDistance(p1, p2);
		//console.log(dist);
		if (dist > 0.5 ){
			return false;
		}
		else{
			return true;
		}
	},

}

smart.ad_validator = new smart.Validator();
