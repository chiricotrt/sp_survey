(function( $ ) {
	var statusMethods = {

		"element.enable": function(){
			this.removeClass('disabled');
		},

		"element.disable": function(){
			this.addClass('disabled');
		},

		"element.enabled": function() {
			return !this.hasClass('disabled');
		},

		"element.disabled": function() {
			return this.hasClass('disabled');
		}
	};

	jQuery.fn.fm('loadModule', statusMethods);


})( jQuery );