if (smart == undefined){
	smart = {};
}

(function ($) {
	smart.timepicker = function(options){
		_.bindAll(this, 'initialize','generateTimeBox',
						'doNothing', 'timeUpdate', 'returnDay',
						'onHourChange', 'onDayChange', 'onMinuteChange',
						'generateDaySelect', 'generateSelect');

		this._default = {
			'validateTime': true,
			'validateMinutes': true
		};
		this.options = $.extend(this._default, options);
		//console.log(this.options)
		this.initialize(options);
	};

	$.extend(smart.timepicker.prototype, {

		initialize: function(){

			this.el = jQuery(this.options.id);
			this.parent = this.el.parent();

			this.currentTime = jQuery(this.el).val();
			//console.log("id",this.options.id, "timepicker currentTime", this.currentTime);
			this.day = parseInt(this.currentTime.split(':')[0],10);
			this.hours = parseInt(this.currentTime.split(':')[1],10);
			this.minutes = parseInt(this.currentTime.split(':')[2],10);
			//console.log("hours", this.hours , "minutes", this.minutes)

			jQuery(this.el).hide();
			if (jQuery('.timepicker_container', this.parent).length > 0)
				return;

			this.container = jQuery('<div class="timepicker_container '+this.options.classes+'" />');
			jQuery(this.parent).append(this.container);
			this.generateTimeBox();

			//jQuery(this.el).bind('change', this.timeUpdate);
		},

		generateTimeBox: function(){
			this.container.html(	this.generateSelect(this.hours, this.options.hours.starts, this.options.hours.ends, true)+":" +
									this.generateSelect(this.minutes, this.options.minutes.starts, this.options.minutes.ends, false) +
									this.generateDaySelect(this.day)
								);

			//console.log(this.parent);

			$('.timepicker_hours', this.container).bind('change', this.onHourChange);
			$('.timepicker_minutes', this.container).bind('change', this.onMinuteChange);
			$('.timepicker_day', this.container).bind('change', this.onDayChange);

			jQuery(this.el).unbind('change', this.timeUpdate);
			jQuery(this.el).bind('change', this.timeUpdate);

			jQuery(this.container).unbind('click', this.doNothing);
			jQuery(this.container).bind('click', this.doNothing);
		},

		resetTimeButtons: function(){
			var topElm = jQuery(this.el).parent().parent();
			jQuery('.change_time_button',topElm).show();
			jQuery('.invalid_button',topElm).hide();
		},

		doNothing: function(e){
			e.preventDefault();
			return false;
		},

		timeUpdate: function(e){
			//console.log('TODO: reflect time in input box in combo',this.currentTime)
			this.currentTime = jQuery(this.el).val();
			this.day = parseInt(this.currentTime.split(':')[0],10);
			this.hours = parseInt(this.currentTime.split(':')[1],10);
			this.minutes = parseInt(this.currentTime.split(':')[2],10);

			this.generateTimeBox();
			time_changed = true;

			//console.log("calling Update On ",$(this.el).attr('id'));
			this.options.onClose($(this.el).val(), {id: $(this.el).attr('id')});

			e.preventDefault();
		},

		returnDay: function(day){
			if (day < 0)
				return '-1';
			else if (day > 0)
				return '1';
			else
				return '0';
		},

		onCommitTime: function(e){
			this.options.onClose($(this.el).val(), {id: $(this.el).attr('id')});
		},

		onHourChange: function(e, notTriggerEvent, preventClose){
			window.time_changed = true;
			//console.log("hour changed", this.hours, this.minutes);
			this.hours = $(e.target).val();
			$('.timepicker_minutes', this.container).replaceWith(this.generateSelect(this.minutes, this.options.minutes.starts, this.options.minutes.ends, false));
			$('.timepicker_minutes', this.container).unbind('change', this.onMinuteChange);

			$('.timepicker_minutes', this.container).bind('change', this.onMinuteChange);
			var ntime = this.returnDay(this.day)+':'+(this.hours<10?'0':'')+this.hours+':'+(this.minutes<10?'0':'')+this.minutes;
			var oldtime = $(this.el).val();
			$(this.el).val(ntime);

			//this.resetTimeButtons();
			updateAccordion(this.options.id.split('_')[2]);


			if (!notTriggerEvent)
				jQuery.publish("timepicker:change", [e.target, oldtime, ntime]);
			//if (!preventClose)
			//	this.options.onClose($(this.el).val(), {id: $(this.el).attr('id')});

			//console.log("hour changed", this.hours, this.minutes, this.day);
		},

		onDayChange: function(e, notTriggerEvent, preventClose){
			window.time_changed = true;
			this.day = $(e.target).val();

			var ntime = this.returnDay(this.day)+':'+(this.hours<10?'0':'')+this.hours+':'+(this.minutes<10?'0':'')+this.minutes;
			var oldtime = $(this.el).val();
			$(this.el).val(ntime);

			//this.resetTimeButtons();
			updateAccordion(this.options.id.split('_')[2]);

			if (!notTriggerEvent)
				jQuery.publish("timepicker:change", [e.target, oldtime, ntime]);
			//if (!preventClose)
			//	this.options.onClose($(this.el).val(), {id: $(this.el).attr('id')});

			//console.log("day changed", this.hours, this.minutes, this.day);
		},

		onMinuteChange: function(e, notTriggerEvent, preventClose){
			window.time_changed = true;
			this.minutes = $(e.target).val();

			//console.log("minutes changed", this.hours, this.minutes);
			//console.log("setting current time - minutes");
			var ntime = this.returnDay(this.day)+':'+(this.hours<10?'0':'')+this.hours+':'+(this.minutes<10?'0':'')+this.minutes;
			var oldtime = $(this.el).val();
			$(this.el).val(ntime);

			// this.resetTimeButtons();
			updateAccordion(this.options.id.split('_')[2]);

			if (!notTriggerEvent)
				jQuery.publish("timepicker:change", [e.target, oldtime, ntime]);


			//console.log("minute changed", this.hours, this.minutes, this.day);
			//if (!preventClose)
			//	this.options.onClose($(this.el).val(), {id: $(this.el).attr('id')});
		},

		normalizedTime: function(time, isHours){
			//if (isHours && time >= 0 && time <=2)
			//	return time + 50

			return time;
		},

		generateDaySelect: function (current){
			var html = '<select class="timepicker_day" >';

			//console.log(current, this.options);

			if (this.options.isFirst){
				html += '<option value="-1" '+(current<0?'selected="selected"':'')+'>of Previous day</option>';
			}
			html += '<option value="0" '+(current==0?'selected="selected"':'')+'>of Current day</option>';
			if (this.options.isLast){
				html += '<option value="1" '+(current>0?'selected="selected"':'')+'>of Next day</option>';
			}

			html += '</select>';
			return html;
		},

		generateSelect: function(current, min, max, isHours){
			//console.log(arguments)
			var currentInsert = false;

			if (this.options.validateTime) {
			// set minutes according to the current hour
				var changedMinMax = false;
				if (!isHours){
					if (this.hours == this.options.hours.starts){
						min = this.options.minutes.starts;
						if (this.options.hours.starts != this.options.hours.ends){
							max = 59;
						}
						changedMinMax= true;
					}
					if (this.hours == this.options.hours.ends){
						if (this.options.hours.starts != this.options.hours.ends){
							min = 0;
						}
						max = this.options.minutes.ends;
						changedMinMax = true;
					}

					if (!changedMinMax || !this.options.validateMinutes ){
						min = 0;
						max = 59;
					}

				} else {
					//console.log("minutes of hour: ",this.minutes, this.options.minutes.starts)
					if (this.minutes < this.options.minutes.starts){
						min = this.options.hours.starts + 1;
						if (min > 23)
							min = min % 23;
					}
				}

				var tmpC = this.normalizedTime(current, isHours);
				var tmpMin = this.normalizedTime(min, isHours);
				var tmpMax = this.normalizedTime(max, isHours);


				if (tmpC < tmpMin || tmpC > tmpMax){
					currentInsert = true;

				}

			}





			var counter = min;
			var lim = max-min;

			if (min > max){
				if (isHours){
					lim = 24-min + max;
				} else {
					lim = 60-min + max;
				}
			}

			//console.log("lim :",lim)
			var html = '<select class="'+(isHours?'timepicker_hours':'timepicker_minutes')+'" >';
			if (currentInsert){
				html += '<option value="'+current+'" selected="selected" >'+(current<10?'0':'')+current+'</option>';
			}

			for (var i=0;i<=lim; i++){
				if (counter > (isHours?23:59))
					counter = 0;

				html += '<option value="'+counter+'" '+(counter == current?'selected="selected"':'')+'>'+(counter<10?'0':'')+counter+'</option>';
				counter++;
			}

			html += '</select>';
			//console.log("html", html);

			return html;
		}
	});
})(jQuery);
