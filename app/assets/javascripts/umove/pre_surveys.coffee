jQuery ->
  setDependencies()
  setAnswers()

setDependencies = ->
  # If difficulty=Yes
  $("#pre_survey_difficulty_in_unfamiliar_cities_1").click ->
    $(".pre_survey_reasons_for_difficulty_in_unfamiliar_cities").show()

  # If difficulty=No
  $("#pre_survey_difficulty_in_unfamiliar_cities_0").click ->
    $("input[name='pre_survey[reasons_for_difficulty_in_unfamiliar_cities][]']").prop('checked', false)
    $(".pre_survey_reasons_for_difficulty_in_unfamiliar_cities").hide()

  # Exclusive answers
  # If licences=None, reset other choices; If licences!=None, reset None.
  $("input[name='pre_survey[driving_licences][]'][value != 3]").click ->
    if $(this).prop("checked")
      $("#pre_survey_driving_licences_3").prop("checked", false)

  $("#pre_survey_driving_licences_3").click ->
    if $(this).prop("checked")
      $("input[name='pre_survey[driving_licences][]'][value != 3]").prop("checked", false)
  
setAnswers = ->
  # if difficulty=yes, show modes
  if $("#pre_survey_difficulty_in_unfamiliar_cities_1").prop("checked") 
    $(".pre_survey_reasons_for_difficulty_in_unfamiliar_cities").show()