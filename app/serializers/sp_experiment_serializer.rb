class SpExperimentSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :completed
end
