class AddRegistrationSourceToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :registration_source, :string
  end

  def self.down
    remove_column :users, :registration_source
  end
end
