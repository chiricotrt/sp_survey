class AddAttitudeTowardsCarsAndCarSharingColumnsToMobilityAttitudes < ActiveRecord::Migration
  def change
    add_column :pq_mobility_attitudes, :attitude_towards_cars_1, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_2, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_3, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_4, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_5, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_6, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_cars_7, :integer

    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_1, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_2, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_3, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_4, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_5, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_6, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_7, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_8, :integer
    add_column :pq_mobility_attitudes, :attitude_towards_car_sharing_9, :integer
  end
end
