class AddPtZonesAndPtDiscountToGeneratedPlans < ActiveRecord::Migration
  def change
    add_column :generated_plans, :pt_zones, :string
    add_column :generated_plans, :pt_discount, :integer
  end
end
