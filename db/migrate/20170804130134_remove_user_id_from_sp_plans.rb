class RemoveUserIdFromSpPlans < ActiveRecord::Migration
  def change
    remove_column :sp_plans, :user_id
  end
end
