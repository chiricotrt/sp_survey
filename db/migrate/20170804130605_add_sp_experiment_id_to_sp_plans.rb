class AddSpExperimentIdToSpPlans < ActiveRecord::Migration
  def change
    add_column :sp_plans, :sp_experiment_id, :integer
  end
end
