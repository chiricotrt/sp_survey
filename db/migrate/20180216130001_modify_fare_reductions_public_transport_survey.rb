class ModifyFareReductionsPublicTransportSurvey < ActiveRecord::Migration
  def change
    remove_column :pre_survey_public_transport_characteristics, :fare_reductions, :string
    add_column :pre_survey_public_transport_characteristics, :fare_reduction, :integer
  end
end
