class UpdateTaxiAndCarQuestionsSharedMobilityQuestionnaire < ActiveRecord::Migration
  def change
    remove_column :pre_survey_shared_mobility_characteristics, :car_club_memberships, :string

    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_1, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_2, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_3, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_4, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_5, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_6, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_7, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_8, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :attitude_towards_uber_9, :integer

    remove_column :pre_survey_shared_mobility_characteristics, :frequency_of_black_cab_hailed, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :frequency_of_black_cab_app, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :frequency_of_uber, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :frequency_of_minicab, :integer
    remove_column :pre_survey_shared_mobility_characteristics, :frequency_of_shared_taxi, :integer

    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_1, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_2, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_3, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_4, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_5, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_6, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_7, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_owner_8, :integer

    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_1, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_2, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_3, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_4, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_5, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_6, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_7, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_8, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_car_sharing_non_owner_9, :integer

    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_taxi_hailed, :integer
    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_taxi_phone, :integer
    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_taxi_app, :integer
    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_taxi_web, :integer

    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_1, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_2, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_3, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_4, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_5, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_6, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_7, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_8, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_9, :integer
    add_column :pre_survey_shared_mobility_characteristics, :attitude_towards_taxi_10, :integer
  end
end
