class AddFlexiblePlanToSpExperiments < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :flexible_plan, :text
  end
end
