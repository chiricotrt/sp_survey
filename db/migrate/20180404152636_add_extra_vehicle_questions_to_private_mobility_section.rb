class AddExtraVehicleQuestionsToPrivateMobilitySection < ActiveRecord::Migration
  def change
    add_column :pre_survey_private_mobility_characteristics, :vehicle_type, :integer
    add_column :pre_survey_private_mobility_characteristics, :vehicle_cost, :float
  end
end
