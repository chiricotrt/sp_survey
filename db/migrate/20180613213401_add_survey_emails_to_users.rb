class AddSurveyEmailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :survey_email_start, :string
    add_column :users, :survey_email_finish, :string
  end
end
