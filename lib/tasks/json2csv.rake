# frozen_string_literal: true

desc 'Converts SP experiment data to CSV format'
task json2csv: :environment do
  sp = CSV.open('tmp/sp_days.csv', 'wb')
  sp_week = CSV.open('tmp/sp_weeks.csv', 'wb')
  rp = CSV.open('tmp/rp.csv', 'wb')

  sp_headers = %w[user_id day travel_time_factor change_plans change_activity activity_choice
               other_activity_choice activity_duration activity_mode other_activity_mode daddloc
               activity_departure_time change_departure_time departure_time_choice change_mode mode_choice
               other_mode_choice change_route route_factor attempted_previously
               adapted_travel_time adjust_schedule adjust_type covid_mode covid_mode_choice
               other_covid_mode]
  sp << sp_headers
  sp_headers_map = Hash[sp_headers.each_with_index.map { |header, i| [header, i] }]

  sp_week_headers = %w[user_id week_no trip_id trip_purpose other_trip_purpose trip_freq trip_mode other_trip_mode
                       evaltype expected_travel_time departure_time arrival_time extend_duration]
  sp_week << sp_week_headers
  sp_week_headers_map = Hash[sp_week_headers.each_with_index.map { |header, i| [header, i] }]

  rp_headers = %w[user_id user_created_at responses_updated_at total_time_seconds ip_address country locale]
  questionnaire = YAML.load_file('config/sections/questionnaire.yml')
  labels = questionnaire.map do |element|
    if element.is_a?(Hash)
      responses_table = element['responses_table']
      responses_table['labels_t'] unless responses_table.nil?
    end
  end

  rp_headers.push(*labels.compact.flatten)
  rp << rp_headers
  rp_headers_map = Hash[rp_headers.each_with_index.map { |header, i| [header, i] }]

  feedback_headers = Set.new(%w[user_id feedback1 feedback2 feedback3 survey_length survey_bearable survey_complexity comments])
  feedback = CSV.open('tmp/feedback.csv', 'wb', headers: feedback_headers.to_a, write_headers: true)


  SpExperiment.all.each do |experiment|
    next if experiment.responses.blank?

    fixed = false
    responses = experiment.responses
    responses['day_choices']&.each do |day, hash|
      if hash.has_key? 'extend_duration'
        hash.delete('extend_duration')
        fixed = true
      end
    end

    responses['day_to_day_learning']&.each do |day, hash|
      if hash.has_key? 'travel_time'
        hash['adapted_travel_time'] = hash['travel_time'] unless hash.has_key?('adapted_travel_time')
        hash.delete('travel_time')
        fixed = true
      end
    end

    %w[introduction introduction2].each do |section|
      section_responses = responses[section]
      if section_responses&.has_key? 'travel_time'
        section_responses['expected_travel_time'] = section_responses['travel_time'] unless section_responses.has_key?('expected_travel_time')
        section_responses.delete('travel_time')
        fixed = true
      end
    end

    if fixed
      experiment.save!
      puts "Fixed SP experiment for user with id #{experiment.user_id}"
    end
  end

  SpExperiment.all.each do |experiment|
    next if experiment.responses.blank?

    rows = Hash.new { |hash, key| hash[key] = [] }
    experiment.responses.each do |section, section_responses|
      case section
      when 'introduction', 'introduction2'
        week_no = section == 'introduction' ? 1 : 2
        row = rows["week#{week_no}"]
        row[sp_week_headers_map['user_id']] = experiment.user_id
        row[sp_week_headers_map['week_no']] = week_no
        row[sp_week_headers_map['evaltype']] = experiment.plans["evaluation#{week_no}"]
        section_responses.each do |name, value|
          if sp_week_headers_map.has_key? name
            row[sp_week_headers_map[name]] = value
          else
            puts "Missing header: #{name}"
          end
        end
      when 'day_choices'
        section_responses.each do |key, hash|
          match = key.match(/day:(\d+)/)
          day = match.captures[0].to_i
          row = rows[day]
          row[sp_headers_map['user_id']] = experiment.user_id

          row[sp_headers_map['day']] = day

          if day <= 6
            times = experiment.plans['travel_times_set1']
            row[sp_headers_map['travel_time_factor']] = SpExperiment::TRAVEL_FACTORS[times][day - 1]
          else
            times = experiment.plans['travel_times_set2']
            row[sp_headers_map['travel_time_factor']] = SpExperiment::TRAVEL_FACTORS[times][day - 7]
          end

          # record day_choices responses
          hash.each do |name, value|
            if sp_headers_map.has_key? name
              row[sp_headers_map[name]] = value
            else
              puts "Missing header: #{name}"
            end
          end
        end

      when 'day_to_day_learning'
        section_responses.each do |key, hash|
          match = key.match(/day:(\d+)/)
          day = match.captures[0].to_i
          row = rows[day]
          hash.each do |name, value|
            if sp_headers_map.has_key? name
              row[sp_headers_map[name]] = value
            else
              puts "Missing header: #{name}"
            end
          end
        end

      when 'questionnaire'
        row = []
        row[rp_headers_map['user_id']] = experiment.user_id
        user = experiment.user
        t0 = row[rp_headers_map['user_created_at']] = user.created_at
        t1 = row[rp_headers_map['responses_updated_at']] = experiment.updated_at
        row[rp_headers_map['total_time_seconds']] = (t1 - t0).to_i
        row[rp_headers_map['ip_address']] = user.ip_address
        results = Geocoder.search user.ip_address
        row[rp_headers_map['country']] = results.first.country
        row[rp_headers_map['locale']] = user.sp_experiment.locale
        section_responses.each do |name, value|
          if rp_headers_map.has_key? name
            row[rp_headers_map[name]] = value
          else
            puts "Missing header: #{name}"
          end
        end

        rp << row

      when 'feedback'
        row = {}
        row['user_id'] = experiment.user_id
        section_responses.each do |name, value|
          if feedback_headers.include? name
            row[name] = value
          else
            puts "Missing header: #{name}"
          end
        end

        feedback << row
      end

    end

    rows.each do |key, row|
      if key.to_i > 0
        sp << row
      else
        sp_week << row
      end
    end

  end

  [sp, sp_week, rp, feedback].each(&:close)
end

