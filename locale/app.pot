# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: app 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-05-12 13:34+0800\n"
"PO-Revision-Date: 2015-05-12 13:34+0800\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

msgid "If you have any questions, please refer to our %{support} page. We want to hear from you!"
msgstr ""

msgid "SMART"
msgstr ""

msgid "Thank you for completing the survey. You should receive your incentive within 4 weeks."
msgstr ""

msgid "homepage.(10 minutes)"
msgstr ""

msgid "homepage.(10-15 minutes)"
msgstr ""

msgid "homepage.(Max 30 minutes per day)"
msgstr ""

msgid "homepage.ACTIVITY DIARY"
msgstr ""

msgid "homepage.Confidentiality"
msgstr ""

msgid "homepage.DONE!"
msgstr ""

msgid "homepage.DOWNLOAD THE APPLICATION"
msgstr ""

msgid "homepage.FAQs"
msgstr ""

msgid "homepage.Help and Support"
msgstr ""

msgid "homepage.Information about you, your home, transportation and housing preferences."
msgstr ""

msgid "homepage.Information about your activities, means of transportation and accompanying persons."
msgstr ""

msgid "homepage.NOT COMPLETE"
msgstr ""

msgid "homepage.PRE-SURVEY"
msgstr ""

msgid "homepage.Please download the application for Android %{android_app} or iPhone %{iphone_app}."
msgstr ""

msgid "homepage.QUESTIONS ABOUT YOU AND YOUR HOUSEHOLD"
msgstr ""

msgid "homepage.SMARTPHONE APP INSTALLATION"
msgstr ""

msgid "homepage.STEP 1"
msgstr ""

msgid "homepage.STEP 2"
msgstr ""

msgid "homepage.STEP 3"
msgstr ""

msgid "homepage.Support"
msgstr ""

msgid "homepage.Tell us about your non-travel and travel activities."
msgstr ""

msgid "homepage.Thank you for your participation!"
msgstr ""

msgid "homepage.The FMS team promises to protect your privacy. To see our privacy policy, please click %{privacy}"
msgstr ""

msgid "homepage.here"
msgstr ""

msgid "layout|ACTIVITY DIARY"
msgstr ""

msgid "layout|CONTACTS"
msgstr ""

msgid "layout|DASHBOARD"
msgstr ""

msgid "layout|HOME"
msgstr ""

msgid "layout|Hello, %{username}!"
msgstr ""

msgid "layout|Logout"
msgstr ""

msgid "layout|PRE-SURVEY"
msgstr ""

msgid "layout|SUPPORT"
msgstr ""

msgid "menu|Chinese"
msgstr ""

msgid "menu|English"
msgstr ""

msgid "menu|Language"
msgstr ""
