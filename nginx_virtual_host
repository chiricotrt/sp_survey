# SSL config for maaspoll.com

upstream maaspoll {
  server unix:/tmp/unicorn.fmsurvey_simple.sock fail_timeout=0;
}

server {
  listen 80;
  server_name maaspoll.com www.maaspoll.com;
  return 301 https://maaspoll.com$request_uri;
}

server {
  listen 443 ssl spdy;
  server_name maaspoll.com www.maaspoll.com;
  root /home/deploy/fmsurvey_simple/current/public;

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;

  ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';

  ssl_certificate_key /etc/letsencrypt/live/maaspoll.com/privkey.pem;

  ssl_stapling on;
  ssl_stapling_verify on;

  ssl_certificate /etc/letsencrypt/live/maaspoll.com/fullchain.pem;

  if (-f $document_root/system/maintenance.html) {
    return 503;
  }
  error_page 503 @maintenance;
  location @maintenance {
    rewrite  ^(.*)$  /system/maintenance.html last;
    break;
  }

  # for webroot renewal of LetsEncrypt certs
  location '/.well-known/acme-challenge' {
    default_type "text/plain";
    root /tmp;
  }

  location ^~ /assets/ {
    gzip_static on;
    add_header Cache-Control public;
    expires 4w;
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_disable "MSIE [1-6]\.";
    gzip_comp_level 6;
    gzip_types  text/plain application/xml text/css text/js text/xml application/x-javascript text/javascript application/json application/xml+rss;
  }

  try_files $uri/index.html $uri @maaspoll;
  location @maaspoll {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X_FORWARDED_PROTO https;

    proxy_redirect off;
    proxy_pass http://maaspoll;
  }

  add_header X-FMServer maaspoll.com;
  add_header Strict-Transport-Security max-age=63072000;

  error_page 500 502 503 504 /500.html;
  client_max_body_size 5m;
  keepalive_timeout 10;
}
